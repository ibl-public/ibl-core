import { Html, Head, Main, NextScript } from 'next/document';

export const siteTitle = 'IBL processes';

export default function Document() {
    return (
        <Html>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <link rel="stylesheet" href=
                    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.css">
                </link>
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={`https://og-image.vercel.app/${encodeURI(
                        siteTitle,
                    )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <meta name="og:title" content={siteTitle} />
                <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <body data-bs-theme="ibl-main">
                <div id="root">
                    <Main />
                    <NextScript />
                </div>
            </body>
        </Html>
    );
}
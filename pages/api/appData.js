import { processConfigs } from "../../lib/components";

const handle = (req, res) =>
    req.method === "GET" ?
        processConfigs().then(result => res.status(200).send(result)) :
        res.status(404);

export default handle;

import { getComponentDataFromInit } from '../../../../lib/components';
import { saveComponentData } from "../../../../services/componentDataService";

const handle = (req, res) =>
{
    const { processId, componentId } = req.query;
    if (req.method === "GET") {
        getComponentDataFromInit({ processId, componentId })
            .then((data) => res.status(200).json({ data }))
            .catch((error) => {
                console.error(`Error getting component init data: ${error}`);
                res.status(404);
            });
    } else if (req.method === "POST") {
        saveComponentData(processId, componentId, JSON.parse(req.body))
            .then(() => res.status(200).json({message: "Saved component data"}))
            .catch(() => res.status(500));
    } else {
        res.status(404);
    }
}

export default handle;

import {writeContent} from "../../services/fileSystem";

const fs = require("fs");
const fileName = "appConfig.json";

const handle = (req, res) =>
{
    if (req.method === "GET") {
        fs.promises.readFile(fileName, "utf-8")
            .then(data => res.status(200).send(data))
            .catch(() => res.status(404))
    } else if (req.method === "POST") {
        writeContent(fileName, req.body)
            .then(() =>
                res.status(200).json({ message: "Saved application data" })
            )
            .catch(() => res.status(500));
    } else {
        res.status(404);
    }
};

export default handle;

import { getLayoutData } from "../../../../lib/components";

const handle = (req, res) =>
{
    if (req.method === "GET") {
        const { processId, componentId } = req.query;

        getLayoutData(processId, componentId)
            .then((result) => res.status(200).send(result));
    } else {
        res.status(404);
    }
};

export default handle;

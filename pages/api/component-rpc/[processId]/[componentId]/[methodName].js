import { getComponent } from "../../../../../services/componentFactory";
import { getUnfilteredComponent } from "../../../../../lib/components"
import formidable from "formidable";

export const config = {
    api: {
        bodyParser: false
    }
};

const getRawBody = (readable) => 
{
    let rawData = Buffer.alloc(0);
    let chunk;
    while ((chunk = readable.read()) !== null) {
        rawData = Buffer.concat([rawData, chunk]);
    }

    return rawData;
};

const handler = (req, res) => 
{
    const { processId, componentId, methodName } = req.query;
    const IBLComponent = getComponent(processId, componentId);
    const unfilteredComponentPromise = getUnfilteredComponent(processId, componentId);    
    
    return unfilteredComponentPromise.then((currentComponent) => {
        if (!IBLComponent) {
            res.status(404);

            return;
        }

        IBLComponent.getAPI
            .then(factory => {
                const api = factory(currentComponent.name);
                if (!api[methodName]) {
                    res.status(404);

                    return;
                }
                try {
                    let result;
                    const contentType = req.headers["content-type"];

                    if (
                        contentType && 
                        contentType.startsWith("multipart/form-data")
                    ) {
                        const form = new formidable.IncomingForm();

                        result = new Promise((resolve, reject) => {
                            form.parse(req, (err, fields, files) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(api[methodName](files.file, currentComponent));
                                }
                            });
                        });
                    } else {
                        const data = JSON.parse(Buffer.from(getRawBody(req)));
                        result = api[methodName](data, currentComponent);
                    }

                    if (!(result instanceof Promise)) {
                        result = new Promise.resolve(result);
                    }
                    result.then(data => res.status(200).json(data)).catch(err =>
                        err && err.code && err.message ?
                            res.status(err.code).json({ error: err.message }) :
                            res.status(500).json({ error: "Internal Server Error" })
                    );
                } catch (e) {
                    res.status(500).json({ error: "Internal Server Error" });
                }
            })
            .catch(_ => res.status(404));
    });
};

export default handler;

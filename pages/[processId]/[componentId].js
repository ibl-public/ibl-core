import { useRouter } from "next/router";
import { useMemo, useState, useEffect } from "react";
import dynamic from "next/dynamic"
import {
    getPublicComponentData,
    getComponentDataFromInit,
    configurationsToMapReducer
} from "../../lib/components";
import { getComponent } from "../../services/componentFactory";
import { useAppContext } from "../../context/AppContext";
import { getEncryptionKey } from "../../services/vaultService";
import { decrypt } from "../../services/cryptoService";
import { post } from "../../lib/apicalls";
import { 
    useSwitchToDefaultNetwork,
    useChainChangedListener
} from "../../lib/metamaskClient";
import InvalidChainComponent from "../../components/InvalidChainComponent";
import IblContractClient from "../../services/IblContractClient";
import { ethers } from "ethers";

const Component = ({ componentData, componentDataFromInit }) =>
{
    const router = useRouter();
    const { processId, componentId } = router.query;
    const [state, dispatch] = useAppContext();
    const [nextStepActive, setNextStepActive] = useState(false);
    const [renderComponent, setRenderComponent] = useState(false);
    const [iblClient, setIblClient] = useState(null);
    const [applicationData, setApplicationData] = useState({});
    const IBLComponent = getComponent(processId, componentId);
    const continueButtonTitle =
        componentData?.nextComponentSelector?.connectionTitle || "Continue";

    const DynamicComponent = useMemo(
        () => dynamic(() => IBLComponent.getAPI
            .then(factory => factory(componentData.name).getComponent())
        ),
        [IBLComponent, componentId]
    );

    useEffect(() =>
        fetch("/api/appData")
            .then(response => response.json())
            .then(data => {
                IblContractClient.initialize(data);
                setIblClient(IblContractClient);
                setApplicationData(data);
            })
            .catch(error => console.error("Error fetching appData:", error)), []);

    const payApplicationFee = () => 
    {
        const componentList = 
            applicationData.processes[0].configuredComponents.map(
                (component) => component.componentId
            );
        
        return iblClient.calculateFeesForRunningApplication(
            componentList, applicationData.applicationId
        ).then((fee) => iblClient.distributeFeesFoRunningApplication(
            componentList, applicationData.applicationId, fee)
        );
    }

    const executeApiCall = (method, data) => post(
        `/api/component-rpc/${processId}/${componentId}/${method}`, data
    );

    useEffect(() => setNextStepActive(false), [componentId]);

    useSwitchToDefaultNetwork(
        state.defaultChain,
        () => setRenderComponent(true),
        (error) => console.error("Error switching Ethereum network:", error)
    );
    
    useChainChangedListener(() => window.location.reload());

    const saveOutput = output => {
        for (const [outputName, outputValue] of Object.entries(output)) {
            dispatch({
                type: "addOutput",
                value: { name: outputName, value: outputValue }
            });
        }

        setNextStepActive(true);
    };

    const buildInputData = () => (componentData?.inputMappings || []).reduce(
        (result, inputMapping) => {
            result[inputMapping.inputName] = state[inputMapping.outputName];

            return result;
        },
        {}
    );

    const navigateToNextComponent = (e) => nextStepActive &&
        router.push(`/${processId}/${componentData.nextComponent}`);

    return renderComponent ? (
        <>
            <DynamicComponent
                saveOutputCallback = { saveOutput }
                configurations = { componentData?.configurations || {} }
                inputData = { buildInputData() }
                componentDataFromInit = { componentDataFromInit }
                callApi = { executeApiCall }
                payApplicationFee = { payApplicationFee }
            />
            {
                componentData.nextComponent &&
                    <button
                        className={ "btn btn-primary" }
                        onClick={ navigateToNextComponent }
                        disabled={ !nextStepActive }
                    >
                        { continueButtonTitle }
                    </button>
            }
        </>
    ) : 
    <InvalidChainComponent />;
};

export const getServerSideProps = (context) =>
{
    const { params } = context;
    let componentData = {};
  
    const defaultDataFromInit = {
        props: { componentData: componentData, componentDataFromInit: {} },
    };
  
    return getPublicComponentData(params)
        .then((data) => {
            componentData = data;
    
            return componentData ?
                getComponentDataFromInit(params) : { notFound: true }
        })
        .then((dataFromInit) => {
            componentData.nextComponent = componentData.nextComponent || null;
    
            return {
                props: {
                    componentData: componentData,
                    componentDataFromInit: dataFromInit,
                },
            };
        })
        .catch((error) => {
            console.error(`Error getting component data: ${error}`);
            
            return defaultDataFromInit;
        });
};

export default Component;

import Head from 'next/head';
import maintenance from "../public/images/maintenance.svg";
import Image from 'next/image';

const Maintenance = () => (
    <>
        <Head>
            <title>Site Maintenance</title>            
        </Head>
        <div className="maintenance-container">
            <h2>Site Maintenance</h2>
            <Image src={ maintenance } alt="maintenance" />
            <p>
                Our site is currently under maintenance. <br/>
                Please check back later.
            </p>
        </div>
    </>
);

export default Maintenance;

import { useEffect } from "react";
import { useRouter } from "next/router";
import { getAllProcesses, getProcessData } from "../lib/components";

const Home = ({ firstProcessData }) =>
{
    const router = useRouter();

    useEffect(
        () => {
            if (firstProcessData) {
                const firstComponentId =
                    firstProcessData?.configuredComponents[0]?.id;

                if (firstComponentId) {
                    router.push(
                        `/${firstProcessData.name}/${firstComponentId}`
                    );
                }
            }
        },
        [firstProcessData]
    );

    return <></>;
};

export const getServerSideProps = () => getAllProcesses()
    .then((processes) => getProcessData(processes[0]?.processId)
        .then((firstProcessData) => ({
            props: { firstProcessData }
        }))
        .catch((error) => {
            console.error(`Error fetching process data: ${error}`);

            return { props: { firstProcessData: null } };
        })
    );

export default Home;

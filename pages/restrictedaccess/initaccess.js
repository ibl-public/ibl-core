import { useAppContext } from "../../context/AppContext";
import Router, { useRouter } from "next/router";
import appData from "../../process-configuration.json";
import appConfig from "../../appConfig.json";
import { useEffect, useState } from "react";

const InitAccess = ({ children }) =>
{
    const [{ account }] = useAppContext();
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(
        () => {
            if (isLoading) {
                setIsLoading(false);
            } else if (
                !account ||
                account?.toLowerCase() !== appData.owner?.toLowerCase()
            ) {
                router.push("/maintenance");
            }
        },
        [account]
    );

    return appConfig.initialized ? <></> : children;
};

export default InitAccess;

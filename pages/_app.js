import "../styles/app.scss";
import { AppWrapper } from "../context/AppContext";
import Layout from "../components/layout";
import InitLayout from "../components/initLayout";
import { useRouter } from 'next/router';
import InitAccess from "./restrictedaccess/initaccess";
import { useState, useEffect } from "react";

const INIT_ROUTES = [
    "/initialize",
    "/initialize/[processId]",
    "/initialize/[processId]/[componentId]",
    "/maintenance"
];

const App = ({ Component, pageProps }) =>
{
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(true);
    const { route } = router;

    useEffect(
        () => {
            fetch("/api/app-config")
                .then(response => response.json())
                .then(appConfig => {
                    if (route.startsWith("/initialize") && appConfig.initialized) {
                        router.push("/")
                            .then(() => setIsLoading(false));
                    } else if (
                        !route.startsWith("/initialize") &&
                        !route.startsWith("/api/") &&
                        !appConfig.initialized
                    ) {
                        router.push("/initialize")
                            .then(() => setIsLoading(false));
                    } else {
                        setIsLoading(false);
                    }
                })
                .catch(e => console.error(e));
        },
        []
    );

    if (isLoading) {
        return <></>;
    }

    return INIT_ROUTES.includes(route) ?
        (
            <AppWrapper>
                <InitAccess>
                    <InitLayout>
                        <Component { ...pageProps } />
                    </InitLayout>
                </InitAccess>
            </AppWrapper>
        ):
        (
            <AppWrapper>
                <Layout>
                    <Component { ...pageProps } />
                </Layout>
            </AppWrapper>
        );
};

export default App;

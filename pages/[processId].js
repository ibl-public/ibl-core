import { getProcessData } from "../lib/components";

const Process = () =>
{
    return <></>;
};

export const getServerSideProps = ({ params }) =>
    getProcessData(params.processId)
        .then((processData) => {
            const components = processData.configuredComponents;

            return (components && components[0].id) ?
                {
                    redirect: {
                        permanent: false,
                        destination: `/${params.processId}/${components[0].id}`
                    }
                } :
                { notFound: true };
        })
        .catch((error) => {            
            console.error(`Error retrieving process data: ${error}`);

            return { notFound: true };
        });

export default Process;

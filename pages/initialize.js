import { useAppContext } from "../context/AppContext";
import { getAllProcesses } from "../lib/components";
import { useState, useEffect } from "react";
import appConfig from "../appConfig.json";
import { useRouter } from "next/router";

const Initialize = ({ processes }) =>
{
    const [{ account }] = useAppContext();
    const [loading, setLoading] = useState(true);
    const router = useRouter();

    useEffect(
        () => { setLoading(false) },
        [account]
    );

    if (loading) {
        return <p>Loading...</p>;
    }

    if (appConfig.currentComponent) {
        router.push(
            "/initialize/" + appConfig.currentComponent.processId + "/" +
            appConfig.currentComponent.componentId
        );
    }

    const nextButtonAction = () =>
        router.push(`/initialize/${processes[0].processId}`);

    return (
        <div>
            <p className="description">
                You will only be required to do this step once.
            </p>
            <div>
                <button
                    className="btn init-btn"
                    type="button"
                    onClick={ nextButtonAction }
                >
                    Start
                </button>
            </div>
        </div>
    );
};

export const getServerSideProps = () => getAllProcesses()
    .then(processes => ({ props: { processes } }));

export default Initialize;

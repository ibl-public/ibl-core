import { getProcessData } from "../../lib/components";
import appConfig from "../../appConfig.json";

const InitializeProcess = () =>
{
    return <></>;
};

export const getServerSideProps = (context) =>
{
    const redirectSettings = {
        redirect: {
            permanent: false,
            destination: "/",
        },
    };
    
    if (appConfig.currentComponent) {
        redirectSettings.redirect.destination = "/initialize/" +
            appConfig.currentComponent.processId + "/" +
            appConfig.currentComponent.componentId

        return Promise.resolve(redirectSettings);
    }
    const processId = context.query.processId;

    return getProcessData(processId)
        .then((processData) => {
            redirectSettings.redirect.destination = "/initialize/" + processId +
            "/" + processData.configuredComponents[0]?.id;

            return redirectSettings;
        })
        .catch(() => ({ notFound: true }))
};
  

export default InitializeProcess;

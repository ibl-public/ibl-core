import { componentData } from "../../../lib/components";
import { getComponent } from "../../../services/componentFactory";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import EmptyInitializeComponent
    from "../../../components/EmptyInitializeComponent";
import appConfig from "../../../appConfig.json";
import React, { useEffect, useState } from "react";
import { post } from "../../../lib/apicalls";
import {
    useSwitchToDefaultNetwork,
    useChainChangedListener
} from "../../../lib/metamaskClient";
import InvalidChainComponent from "../../../components/InvalidChainComponent";
import { useAppContext } from "../../../context/AppContext";

const Component = ({ processes }) =>
{
    const router = useRouter();
    const { processId, componentId } = router.query;
    const [componentInitData, setComponentInitData] = useState({});
    const [nextStepActive, setNextStepActive] = useState(false);
    const [renderComponent, setRenderComponent] = useState(false);
    const [state, dispatch] = useAppContext();
    const currentProcess = processes.find(process => process.uri === processId);
    const component = currentProcess.configuredComponents.find(
        component => component.id === componentId
    );

    useEffect(
        () => {
            fetch(`/api/component-init-data/${processId}/${componentId}`)
                .then(response => response.json())
                .then(json => setComponentInitData(json.data))
                .catch(error => console.error(error))
        },
        []
    );

    useSwitchToDefaultNetwork(
        state.defaultChain,
        () => setRenderComponent(true),
        (error) => console.error("Error switching Ethereum network:", error)
    );

    useChainChangedListener(() => window.location.reload());

    useEffect(() => setNextStepActive(false), [componentId]);

    const InitializerComponent = dynamic(() =>
        getComponent(processId, componentId).getAPI
            .then(getComponentApi => getComponentApi(component.name))
            .then(api => (!api || typeof api.initialize !== "function") ?
                EmptyInitializeComponent : api.initialize()
            )
    );

    const executeApiCall = (method, data) => post(
        `/api/component-rpc/${processId}/${componentId}/${method}`, { data }
    );

    const saveAppConfig = () => post("/api/app-config", appConfig);

    const saveInitData = (data) =>
        post(`/api/component-init-data/${processId}/${componentId}`, data);

    const getStep = () => nextStepActive ? -1 : appConfig.currentStep || 0;

    const setStepAndSaveData = (step) =>
    {
        appConfig.currentStep = step;

        return saveAppConfig();
    };

    const goToNextComponent = () =>
    {
        if (!nextStepActive) {
            return;
        }

        let isLast = false;
        const processConfigurations = currentProcess.configuredComponents;
        const currentComponentIndex = processConfigurations
            .findIndex((component) => component.id === componentId);
        const currentProcessIndex =
            processes.findIndex((process) => process.uri === processId);

        let nextProcessId = processId;
        let nextComponentId = null;

        if (currentComponentIndex === processConfigurations.length - 1) {
            if (currentProcessIndex === processes.length - 1) {
                isLast = true;
            } else {
                const nextProcess = processes[currentProcessIndex + 1];
                nextProcessId = nextProcess.uri;
                nextComponentId = nextProcess.configuredComponents[0].id;
            }
        } else {
            nextComponentId =
                processConfigurations[currentComponentIndex + 1].id;
        }

        appConfig.initialized = isLast;
        appConfig.currentStep = isLast ? undefined : 0;
        appConfig.currentComponent = isLast ? undefined :
            { processId: nextProcessId, componentId: nextComponentId };

        return saveAppConfig().then(() => router.push(
            isLast ? "/" : `/initialize/${nextProcessId}/${nextComponentId}`
        ));
    }

    const onEndComponentInit = () => setNextStepActive(true);

    return renderComponent ? (
        <>
            <p className="description">{ component.name }</p>
            {
                componentInitData && (
                    <InitializerComponent
                        getStep = { getStep }
                        setStep = { setStepAndSaveData }
                        callApi = { executeApiCall }
                        setData = { saveInitData }
                        getData = { () => componentInitData }
                        endComponentInit = { onEndComponentInit }
                    />
                )
            }
            <div>
                <button
                    className="btn init-btn"
                    type="button"
                    hidden={ !nextStepActive }
                    onClick={ () => goToNextComponent() }
                >
                    Continue
                </button>
            </div>
        </>
    ) :
    <InvalidChainComponent />;
}

export const getServerSideProps = () =>
    componentData()
        .then((result) => ({ props: { processes: result.processes } }))
        .catch((error) => {
            console.error("Error fetching component data:", error);

            return { props: { processes: [] } };
        });

export default Component;

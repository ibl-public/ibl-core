const fs = require("fs").promises;

const getEncryptionKey = () => fs.readFile("/vault/secrets/config.txt", "utf8")
    .then((vaultData) => JSON.parse(vaultData).encryptionKey)
    .catch(_ => "aa276bea5b7d70b2484477bfa4e43fc1");

module.exports = {
    getEncryptionKey
};

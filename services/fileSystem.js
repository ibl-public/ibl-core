const fs = require("fs");

const writeContent = (path, content, overwrite = true) =>
{
    const flag = overwrite ? "w" : "a";

    return fs.promises.writeFile(path, content, { flag })
        .then(() => console.log(`Successfully wrote to file: ${path}`))
        .catch((err) =>
            console.error(`Error occurred when writing file ${path}: ${err}`)
        );
};

const mergeDataInFile = (file, newData) =>
    fs.promises.readFile(file, "utf-8")
        .then((existingData) => {
            const parsedData = JSON.parse(existingData);
            const extendedData = { ...parsedData, ...newData };

            return writeContent(file, JSON.stringify(extendedData));
        })
        .catch(() => writeContent(file, JSON.stringify(newData)));

const ensureFolder = (path) => fs.promises.access(path)
    .catch(() => fs.promises.mkdir(path, { recursive: true }));


const readFile = (path) => fs.promises.access(path)
    .then(() => fs.promises.readFile(path))
    .then((data) => JSON.parse(data))
    .catch((error) => {
        console.error(`Error accessing ${path}: ${error}`);

        return {};
    });

module.exports = { writeContent, mergeDataInFile, ensureFolder, readFile };

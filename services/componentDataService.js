import { mergeDataInFile, ensureFolder } from "./fileSystem";

const dataFolder = "componentData";

export const saveComponentData = (processId, componentId, newData) =>
{
    if (processId && componentId) {
        return ensureFolder(`${dataFolder}/${processId}`)
            .then(() => mergeDataInFile(
                `${dataFolder}/${processId}/${componentId}.json`, newData
            ));
    }

    throw new Error("Invalid process id or component id");
};

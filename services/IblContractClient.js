import { ethers } from "ethers";
import ibl from "../ethereum/ibl";
import iblERC20 from "../ethereum/iblERC20";

class IblContractClient
{
    constructor() {
        this.provider = null;
        this.iblContract = null;
        this.iblERC20 = null;
    }

    initialize = (appData) => 
    {
        this.provider = new ethers.providers.Web3Provider(window.ethereum);

        this.iblContract = ibl(
            this.provider.getSigner(),
            appData.iblAddress
        );
        this.iblERC20 = iblERC20(
            this.provider.getSigner(),
            appData.iblErc20Address
        );
    }

    calculateFeesForRunningApplication = (componentId, applicationId) =>
        this.iblContract.calculateFeesFoRunningApplication(
            componentId, applicationId
        ).catch(error => {
            throw new Error("Error calculating application fee:", error);
        });

    distributeFeesFoRunningApplication = (componentIds, applicationId, appFee) =>
        this.iblContract.distributeFeesFoRunningApplication(
            componentIds, applicationId, { value: appFee }
        ).then((tx) => tx.wait()).catch(error => {
            throw new Error("Error distributing fees:", error);
        });
}

export default new IblContractClient();

import {
    createContext,
    useContext,
    useMemo,
    useReducer,
    useEffect
} from "react";
import { AppReducer, initialState } from "./AppReducer";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const { ethereum } = typeof window !== "undefined" ? window : {};

const AppContext = createContext();

export const AppWrapper = ({ children }) =>
{
    const [appState, dispatch] = useReducer(AppReducer, initialState);

    const setAccount = (account) =>
        dispatch({ type: "setAccount", value: account });

    const setDefaultChain = (chainId) =>
        dispatch({ type: "setDefaultChain", value: chainId });

    const connectAndRetrieveAccount = () =>
    {
        if (!ethereum) {
            toast.error("Please install MetaMask.");
            setAccount(undefined);

            return;
        }

        const addresses = ethereum.selectedAddress ?
            Promise.resolve([ethereum.selectedAddress]):
            ethereum.request({ method: "eth_requestAccounts" })
                .then(() => ethereum.request({ method: "eth_accounts" }));

        addresses.then((accounts) => setAccount(accounts[0]))
            .catch((err) => console.error(err.message));
    };

    const contextValue = useMemo(
        () => [appState, dispatch, connectAndRetrieveAccount],
        [appState, dispatch , connectAndRetrieveAccount]
    );

    useEffect(
        () => {
            connectAndRetrieveAccount();
            const state = JSON.parse(localStorage.getItem("appState"));
            state && dispatch({type: "getInitialState", value: state});

            fetch("/api/appData")
                .then(response => response.json())
                .then((data) => setDefaultChain(data.defaultBlockChain));

            if (ethereum && typeof ethereum?.on === "function") {
                ethereum.on("accountsChanged", connectAndRetrieveAccount);
            }

            return () => {
                ethereum && typeof ethereum?.removeListener === "function" &&
                    ethereum.removeListener(
                        "accountsChanged", connectAndRetrieveAccount
                    );
            };
        },
        []
    );

    useEffect(
        () => {
            appState !== initialState &&
            localStorage.setItem("appState", JSON.stringify(appState));
        },
        [appState]
    );

    return (
        <AppContext.Provider value={ contextValue }>
            <ToastContainer />
            { children }
        </AppContext.Provider>
    );
};

export const useAppContext = () => useContext(AppContext);

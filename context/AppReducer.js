export const initialState = { account: null, defaultChain: null };

export const AppReducer = (state, action) =>
{
    switch (action.type) {
        case "setAccount":
            return { ...state, account: action.value };
        case "setDefaultChain":
            return { ...state, defaultChain: action.value };
        case "getInitialState":
            return action.value;
        case "addOutput": {
            return { ...state, [action.value.name] : action.value.value };
        }
        default:
            return action.value;
    }
};

const fs = require("fs");
const themeConfig = require("../process-configuration.json");
const theme = themeConfig.theme;

const importStatements = theme.imports.map(importPath =>
    `@import "${theme.packageName}${importPath}";`).join("\n");

const applyTheme = () =>
{
    const writeStream = fs.createWriteStream(
        "styles/app.scss", { flags: "a", encoding: "utf-8" }
    );

    writeStream.write(`\n${importStatements}\n`);
    writeStream.end();

    writeStream.on("finish", () => console.log("Theme applied successfully!"));
    writeStream.on(
        "error", error => console.error(`Error applying theme: ${error}`)
    );
};

applyTheme();

const writeContent = require("../services/fileSystem").writeContent;
const componentData = require("../process-configuration.json");
const fs = require("fs");

const mainContent = `import dynamic from "next/dynamic"

let processMap = {};

export const getComponent = (process, component) =>
    processMap[process] !== undefined && processMap[process][component];
    
export const getComponentWithoutProcess = (componentName) =>
{
    for (const process in processMap) {
        if (processMap[process].hasOwnProperty(componentName)) {
            return processMap[process][componentName];
        }
    }
    
    return null;
}
`;

const processedProcesses = []

const importStatement = ( process, component, importFrom ) =>
{
    let content = "";
    if (!processedProcesses.includes(process)) {
        content = `\nprocessMap["${process}"] = {};\n`
        processedProcesses.push(process);
    }

    return content.concat(
        `\nprocessMap["${process}"]["${component}"] = {                
            "getAPI" : import("${importFrom}").then(module => module.getAPI)
        }\n`
    );
}

const generateComponentFactory = () =>
{
    const directory = "services";
    const path = `${directory}/componentFactory.js`;
    try {
        !fs.existsSync(directory) && fs.mkdirSync(directory);
    } catch (err) {
        console.error(`Failed to create directory ${directory}: ${err}`);

        return;
    }

    writeContent(path, mainContent)
        .then(() => componentData.processes.forEach((process) =>
            process.configuredComponents.forEach((component, index) =>
                writeContent(
                    path,
                    importStatement(
                        process.uri,
                        component.name + "-" + index,
                        component.importPath
                    ),
                    false
                )
            )
        ));
}

generateComponentFactory();

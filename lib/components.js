import { getEncryptionKey } from "../services/vaultService";
import { decrypt } from "../services/cryptoService";
import { readFile } from "../services/fileSystem";

export const processConfigs = () => readFile("process-configuration.json");

export const getAllProcesses = () => processConfigs().then(processConfigs =>
    processConfigs.processes.map((process) => ({ processId: process.uri }))
);

export const getProcessData = (processId) => componentData().then((data) => {
    const processes = data.processes.filter(p => p.uri === processId);

    if (!processes) {
        throw new Error("Process not found");
    }

    return {
        name: processId,
        configuredComponents: processes[0].configuredComponents
    }
});

export const getPublicComponentData = ({ processId, componentId }) =>
    getProcessAndComponent(processId, componentId)
        .then(({ process, component }) => {
            const positionOfCurrent = process.configuredComponents
                .map(component => component.id)
                .indexOf(component.id);

            return getFilteredComponent(
                component,
                positionOfCurrent === -1 ? null :
                    process.configuredComponents[positionOfCurrent + 1]?.id
            );
        });

export const getLayoutData = (processId, componentId) =>
    !processId || !componentId ? Promise.resolve({}) :
        Promise.all([getProcessData(processId), processConfigs()])
            .then(([processData, processConfig]) => {
                const components = processData.configuredComponents;

                return components ?
                    {
                        isFirstComponent: components[0].id === componentId,
                        logoName: processConfig.logoName,
                        appName: processConfig.metaTitle
                    } :
                    {};
            });

export const getComponentDataFromInit = ({ processId, componentId }) =>
{
    if (!processId || !componentId) {
        throw new Error("Invalid process id or component id");
    }

    return readFile(`componentData/${processId}/${componentId}.json`);
};

export const componentData = () => processConfigs().then(processConfigs => {
    const processes = processConfigs.processes;
    const decryptConfigurationsPromises = processes.map((process) =>
        Promise.all(
            process.configuredComponents.map((component, index) => {
                component.id = component.name + "-" + index;

                return decryptConfigurations(component.configurations)
                    .then((decryptedConfigurations) =>
                        component.configurations = decryptedConfigurations
                    );
            })
        )
    );

    return Promise.all(decryptConfigurationsPromises)
        .then(() => processConfigs);
});

const areEncrypted = (configurations) => typeof configurations === "object" &&
    Object.keys(configurations).length === 2 &&
    "data" in configurations && "iv" in configurations;

const decryptConfigurations = (configurations) =>
    !configurations || !areEncrypted(configurations) ?
        Promise.resolve(configurations) :
        getEncryptionKey()
            .then((secretKey) => decrypt(configurations, secretKey))
            .catch((error) => {
                throw new Error(
                    `Error decrypting configurations: ${error}`,
                    configurations
                );
            });

const getFilteredComponent = (component, nextComponent = null) =>
{
    const configurations = (component.configurations || []);
    const dto = componentToDto(
        component,
        configurations.filter === undefined ? configurations :
            configurations.filter(configuration => !configuration.private)
    );
    dto.nextComponent = nextComponent;

    return dto;
}

export const getUnfilteredComponent = (processId, componentId) =>
    getProcessAndComponent(processId, componentId).then(
        ({ component }) => componentToDto(component, component.configurations)
    );


const componentToDto = (component, configurations) => ({
    id: component.id,
    name: component.name,
    importPath: component.importPath,
    inputMappings: component.inputMappings,
    configurations: configurations.reduce === undefined ? configurations :
        configurations.reduce(configurationsToMapReducer, {}),
    output: component.output,
    nextComponentSelector: component.nextComponentSelector || {}
});

export const configurationsToMapReducer = (result, configuration) =>
{
    result[configuration.machineName] = configuration.value;

    return result;
};

const getProcessAndComponent = (processId, componentId) =>
    getProcessData(processId).then((process) => {
        const component = process.configuredComponents
            .find((component) => component.id === componentId);

        if (!component) {
            const errorMessage = `Component with ID ${componentId} not found.`;
            console.error(errorMessage);

            throw new Error(errorMessage);
        }

        return { process, component };
    });

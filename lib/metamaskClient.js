import { useEffect } from "react";

const switchToDefaultNetwork = (defaultChainId) =>
{    
    if (window.ethereum && window.ethereum.chainId !== defaultChainId) {
        return window.ethereum.request({
            method: "wallet_switchEthereumChain",
            params: [{ chainId: defaultChainId }]
        }).catch((switchError) => {
            switchError.code === 4902 &&
                window.ethereum.request({
                    method: "wallet_addEthereumChain",
                    params: [{
                        chainId: defaultChainId,
                        chainName: "Sepolia test network",
                        rpcUrls:["https://sepolia.infura.io/v3/"],
                        blockExplorerUrls: ["https://sepolia.etherscan.io"],
                        nativeCurrency: {
                            symbol: "SepoliaETH",
                            decimals: 18
                        }
                    }]
                });

            return Promise.reject();
        });
    } else {
        return Promise.resolve();
    }
};

const addNetworkListener = (defaultChainId) =>
    window.ethereum && window.ethereum.on(
        "chainChanged", () => switchToDefaultNetwork(defaultChainId)
    );

export const useSwitchToDefaultNetwork = (
    defaultChain, 
    onSwitchSuccess, 
    onSwitchError
) =>
    useEffect(() => {
        if (defaultChain) {
            switchToDefaultNetwork(defaultChain)
                .then(() => { onSwitchSuccess && onSwitchSuccess() })
                .catch((error) => { onSwitchError && onSwitchError(error) });
        }
    }, [defaultChain, onSwitchSuccess, onSwitchError]);

export const useChainChangedListener = (onChainChanged) =>
    useEffect(() => { 
        window.ethereum && window.ethereum.on(
            "chainChanged", 
            () => onChainChanged && onChainChanged()
        )
    }, [onChainChanged]);

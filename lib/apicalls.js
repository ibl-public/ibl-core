export const post = (url, data) =>
{
    if (data instanceof File) {
        const formData = new FormData();
        formData.append("file", data);
        
        return fetch(url, { method: "POST", body: formData })
            .catch((e) => console.error(`Error sending file to ${url}: ${e}`));
    }

    return fetch(url, { method: "POST", body: JSON.stringify(data) })
        .catch((e) => console.error(`Error sending data to ${url}: ${e}`));
};

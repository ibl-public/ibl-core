import { ethers } from "ethers";
const { abi } = 
    require( "../abi/IBLERC20.sol/IBLERC20.json");

export default (signerOrProvider, address) =>
    new ethers.Contract(address, abi, signerOrProvider);

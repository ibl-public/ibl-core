import { ethers } from "ethers";
const { abi } = require( "../abi/IBL.sol/IBL.json");

export default (signerOrProvider, address) =>
    new ethers.Contract(address, abi, signerOrProvider);

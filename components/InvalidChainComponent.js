const InvalidChainComponent = () => (
    <p className="description">
        Component cannot be rendered as you are not on the required blockchain.
    </p>
);

export default InvalidChainComponent;

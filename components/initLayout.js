import { useRouter } from 'next/router';
import Image from 'next/image';
import wizard from "../public/images/wizard.svg";

const InitLayout = ({ children }) =>
{
    const router = useRouter();

    return (
        <main>
            <div>
                {
                    router.route.includes("/maintenance") ?
                        children :
                        <div className="init-container">
                            <div className="container">
                                <div className="row">
                                    <div className="col col-md-8 text-col">
                                        <p className="title">
                                            Initialize Wizard
                                        </p>
                                        { children }
                                    </div>
                                    <div className="col col-md-4 img-col">
                                        <Image src={ wizard } alt="wizard" />
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </div>
        </main>
    );
};

export default InitLayout;

import { useEffect } from "react";

const EmptyInitializeComponent = ({ endComponentInit }) =>
{
    useEffect(endComponentInit, []);

    return (
        <p className="description">
            This component does not need initialization
        </p>
    );
};

export default EmptyInitializeComponent;

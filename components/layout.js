import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { useEffect, useState } from "react";

const Layout = ({ children }) =>
{
    const router = useRouter();
    const { processId, componentId } = router.query;
    const [logoName, setLogoName] = useState(null);
    const [applicationName, setApplicationName] = useState(null);
    const [isFirstComponent, setIsFirstComponent] = useState(null);

    useEffect(
        () => {
            fetch(`/api/layout-data/${processId}/${componentId}`)
                .then(response => response.json())
                .then((data) => {
                    setLogoName(data.logoName);
                    setApplicationName(data.appName);
                    setIsFirstComponent(data.isFirstComponent);
                });
        },
        []
    );

    return (
        <main className="app"  data-bs-theme="ibl-main">
            <div className="ibl container">
                <header>
                    <Link href="/" className="d-flex align-items-center">
                        <div>
                            {
                                logoName &&
                                    <Image
                                        priority
                                        src={ "/images/" + logoName }
                                        width={75}
                                        height={75}
                                        alt=""
                                    />
                            }
                            <h1>{ applicationName || "IBL" }</h1>
                        </div>
                    </Link>
                </header>
                <div className="application-wrapper container">
                    {
                        !isFirstComponent &&
                            <button
                                className="btn ibl-bg back-home"
                                type="button"
                                onClick={() => router.push("/")}
                            >
                                Back to home
                            </button>
                    }
                    { children }
                </div>
            </div>
        </main>
    );
};

export default Layout;
